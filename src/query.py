class Query:
    alphabet = 'ACGT'
    qual_abc = '!"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~'

    def __init__(self, name: str = '', content: str = '', qual: str = ''):
        self.name = name
        self.content = content
        self.qual = qual
        self.l = len(content)

    def __repr__(self):
        return '@{}\n{}\n+\n{}\n'.format(self.name, self.content, self.qual)
