class Gene:
    alphabet = 'ACGT'

    def __init__(self, name: str = '', content: str = ''):
        self.content = content
        self.l = len(content)
        self.name = name

    def __str__(self):
        return '>{}\n{}\n'.format(self.name, self.content)


if __name__ == '__main__':
    from utils import parse_fa_file
    for g in parse_fa_file('sample.fa'):
        print(g, end='')
