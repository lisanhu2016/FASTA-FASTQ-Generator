from utils import gen_gene
from gene import Gene


def gen_gene_with_range(rng: [int, int], num: int) -> [Gene]:
    for i in range(num):
        import random as rnd
        l = rnd.randint(rng[0], rng[1])
        yield gen_gene(str(i), l)


def gen_gene_with_length(length: int, num: int) -> [Gene]:
    for i in range(num):
        yield gen_gene(str(i), length)


if __name__ == '__main__':
    import argparse as ap

    parser = ap.ArgumentParser(description="Generating .fa file")
    parser.add_argument('--range', nargs=2, type=int,
                        help='length range of the genes, --length will be ignored when this is set')
    parser.add_argument('--length', type=int, default=5000, help='length of the genes')
    parser.add_argument('--num', type=int, default=100, help='Number of genes generated, 100 by default')
    parser.add_argument('output', metavar='<output>', type=str, help='output file name')
    args = parser.parse_args()
    print(args)

    out = open(args.output, 'w')
    if args.range is not None:
        for g in gen_gene_with_range(args.range, args.num):
            out.write(str(g))
    else:
        for g in gen_gene_with_length(args.length, args.num):
            out.write(str(g))
