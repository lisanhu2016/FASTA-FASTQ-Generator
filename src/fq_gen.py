from utils import gen_query
from utils import parse_fa_file
from query import Query
from gene import Gene


def gen_query_with_range(rng: [int, int], num: int, genes: [Gene]) -> [Query]:
    for i in range(num):
        import random as rnd
        l = rnd.randint(rng[0], rng[1])
        yield gen_query(str(i), l, genes)


def gen_query_with_length(length: int, num: int, genes: [Gene]) -> [Query]:
    for i in range(num):
        yield gen_query(str(i), length, genes)


if __name__ == '__main__':
    import argparse as ap

    parser = ap.ArgumentParser(description="Generating .fq file from .fa file")
    parser.add_argument('--range', nargs=2, type=int,
                        help='length range of the queries, --length will be ignored when this is set')
    parser.add_argument('--length', type=int, default=100, help='length of the queries')
    parser.add_argument('--num', type=int, help='Number of queries generated, 10000 by default', default=10000)
    parser.add_argument('input', metavar='<input>', type=str, help='input file name')
    parser.add_argument('output', metavar='<output>', type=str, help='output file name')
    # args = parser.parse_args('out.fa out.fq'.split())
    args = parser.parse_args()
    print(args)

    genes = list(parse_fa_file(args.input))
    out = open(args.output, 'w')
    if args.range is not None:
        for g in gen_query_with_range(args.range, args.num, genes):
            out.write(str(g))
    else:
        for g in gen_query_with_length(args.length, args.num, genes):
            out.write(str(g))
