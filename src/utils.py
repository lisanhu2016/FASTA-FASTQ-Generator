from gene import Gene
from query import Query
import io


def gen_gene(name: str, length: int = None) -> Gene:
    import random as rnd
    g = Gene()
    if length is None:
        g.l = rnd.randint(50, 150)
    else:
        g.l = length
    g.content = ''.join(rnd.choices(Gene.alphabet, k=g.l))
    g.name = name
    return g


def read_next_gene(stream: io.FileIO) -> Gene:
    content = None
    name = None
    for line in stream:
        line_bytes = line
        line = line.replace(b'\r\n', b'\n')
        line = line.decode()
        if line.startswith('>'):
            if content is not None and name is not None:
                g = Gene(name, content)
                stream.seek(-len(line_bytes), io.SEEK_CUR)
                return g
            name = line[1:-1]
        else:
            if content is None:
                content = ''
            content += line[:-1]
    if content is not None and name is not None:
        return Gene(name, content)
    return None


def parse_fa_file(path: str) -> [Gene]:
    f = io.FileIO(path)
    g = read_next_gene(f)
    while g is not None:
        yield g
        g = read_next_gene(f)
    f.close()


def gen_query(name: str, length: int = None, genes: [Gene] = None) -> Query:
    import random as rnd
    q = Query()
    if length is None:
        q.l = rnd.randint(50, 150)
    else:
        q.l = length
    if genes is None:
        q.content = ''.join(rnd.choices(Query.alphabet, k=q.l))
    else:
        g = rnd.choice(genes)
        while g.l < q.l:
            g = rnd.choice(genes)
        start = rnd.randint(0, g.l - q.l)
        q.content = g.content[start:start + q.l]
    q.qual = ''.join(rnd.choices(Query.qual_abc, k=q.l))
    q.name = name
    return q
