def test_yield():
    for i in range(10):
        yield i
        break
    yield None


def test():
    ans = test_yield()
    print(type(ans))
    for i in ans:
        print(i)


if __name__ == '__main__':
    test()
    from sys import stdout
    print(type(stdout))
